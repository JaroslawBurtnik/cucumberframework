Feature: Login Feature
  This feature deals with login functionality of the application

  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the following for login:
      | username | password |
      | userName | passWord |
    And I click login button
    And I should see the userform page


#  Scenario Outline: Login with correct username and password using Scenario Outline
#    Given I navigate to the login page
#    And I enter the users email address as Email:admin
#    And I verify the count of my salary digits for Rs 1000
#    And I enter <username> and <password> for Login:
#    And I click login button
#    And I should see the userform page
#
#    Examples:
#      | username | password   |
#      | execute  | automation |
#      | admin    | admin      |
#      | testing  | qa         |