package steps;

import Base.BaseUtil;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import pages.LoginPage;
import transformation.EmailTransform;
import transformation.SalaryCountTransformer;
import org.junit.Assert;


import java.util.ArrayList;
import java.util.List;


public class LoginStep extends BaseUtil {

    private BaseUtil base;

    public LoginStep(BaseUtil base) {
        this.base = base;
    }

    @And("I should see the userform page")
    public void iShouldSeeTheUserformPage() {

//        System.out.println("The driver is : " + base.Driver + "\n");
//
//        System.out.println("I should see userform page\n");

        Assert.assertEquals(base.Driver.findElement(By.id("Initial")).isDisplayed(), true);
    }

    @Given("I navigate to the login page")
    public void iNavigateToTheLoginPage() {
        System.out.println("Navigate to login page\n");
        base.Driver.navigate().to("http://executeautomation.com/demosite/Login.html");

    }


    @And("I click login button")
    public void iClickLoginButton() {
//        System.out.println("Click login button\n");
//        base.Driver.findElement(By.name("Login")).submit();  // -> submit because on the website this button has type 'submit'

        LoginPage page = new LoginPage(base.Driver);
        page.ClickLogin();


    }

    @And("I enter the following for login:")
    public void iEnterTheFollowingForLogin(DataTable table) {
        List<List<String>> data = table.raw();

        /*System.out.println("The value is : " + data.get(0).get(0).toString());
        System.out.println("The second value is : " + data.get(0).get(1).toString());*/

        //Create an Arraylist
        List<User> users = new ArrayList<User>();
        //Store all the users
        users = table.asList(User.class);

        // were used in previuos lessons
       /* for (User user: users) {
            System.out.println("The username is " + user.username);
            System.out.println("The password is " + user.password);
        }*/

        LoginPage page = new LoginPage(base.Driver);

       for (User user: users) {
//           base.Driver.findElement(By.name("UserName")).sendKeys(user.username);   // - no longer necessary while automating process via pages package
//           base.Driver.findElement(By.name("Password")).sendKeys(user.password);   // - no longer necessary while automating process via pages package

           page.Login(user.username, user.password);
       }
    }

    @And("^I enter ([^\"]*) and ([^\"]*) for Login:$")
    public void iEnterUsernameAndPasswordForLogin(String username, String password) {
        System.out.println("\nUsername is: " + username + " ");
        System.out.println("Password is: " + password + "\n");
    }

    @And("^I enter the users email address as Email:([^\"]*)$")
    public void iEnterTheUsersEmailAddressAsEmailAdmin(@Transform(EmailTransform.class) String email) {
        System.out.println("The e-mail address is: " + email + "\n");

    }
//
//    @And("^I verify the count of my salary digits for Rs (\\d+)$")
//    public void iVerifyTheCountOfMySalaryDigitsForRs(@Transform(SalaryCountTransformer.class) int salary) {
//        System.out.println("My salary digits count is: " + salary);
//    }

    public class User {
        public String username;
        public String password;

        public User(String userName, String passWord) {
            this.username = userName;
            this.password = passWord;
        }

    }

}
