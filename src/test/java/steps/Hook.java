package steps;

import Base.BaseUtil;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Hook extends BaseUtil {

    private BaseUtil base;

    public Hook(BaseUtil base) {
        this.base = base;
    }

    @Before
    public void InitializeTest() {
//        System.out.println("Opening the browser: Chrome\n");

        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // following two lines of codes are made to prevent error "failed to load extension from... Loading of unpacked extensions is disabled by the administrator.//
        // however you will not be able to perform any window resizing/positioning operations without the Chrome automation extension.                              //
        ChromeOptions options = new ChromeOptions();                                                                                                                //
        options.setExperimentalOption("useAutomationExtension", false);                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        base.Driver = new ChromeDriver(options); // (options) if two lines of code above are uncommented then the parameter for the ChromeDriver should be 'options'

    }

    @After
    public void TearDwonTest(Scenario scenario) {

        if (scenario.isFailed()) {
            //Take a screenshot implementation can be done here for example
            System.out.println(scenario.getName());
        }
        System.out.println("Closing the browser : MOCK\n");
    }

}